module Main where

import Trouble

main :: IO ()
main = do
  putStr "Build a rocket... "
  print $ rocket [(120,20),(10,2),(60,14),(90,30),(5,2),(10000,9999)]
  putStrLn ""
