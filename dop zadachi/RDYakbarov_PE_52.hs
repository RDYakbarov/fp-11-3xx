import Data.List
figure_same a b = (show a) \\ (show b) == []

check n = all (figure_same n) (map (n*) [2..6])

result = head $ filter check [1..]

