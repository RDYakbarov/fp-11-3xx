revers = read . revers . show
 palindrom x =
    ax == reverse ax
    where
    ax = show x 
 lychrel = 
    not . any palindrom . take 50 . tail . iterate next
    where
    next x = x + revers x
result = length $ filter lychrel [1..10000]
